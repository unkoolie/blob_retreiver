# -*- coding: utf-8 -*-
from django import forms



class BlobRetrieveForm(forms.Form):
     account_name = forms.CharField(max_length=100)
     queuename = forms.CharField(max_length=50)
     account_key = forms.CharField(max_length=150)