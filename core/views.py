# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from azure.storage.queue import QueueService
from django.shortcuts import render
from .forms import BlobRetrieveForm
from django.views.generic import FormView

# Create youaccount_namer views here.


class BlobRetreiverView(FormView):
    '''
        The form takes in the required values and
        gives the blob link back
    '''

    template_name = 'core/blob_retreiver.html'
    form_class = BlobRetrieveForm

    def get_blob_link(self, account_key, queuename, account_name):
        queue_service = None
        queue_service = QueueService(
            account_name=account_name,
            account_key=account_key)
        if queue_service.exists(queuename):
            metadata = queue_service.get_queue_metadata(queuename)
            count = metadata.approximate_message_count
            if (count > 0):
                messages = queue_service.get_messages(
                    queuename,
                    num_messages=16,
                    visibility_timeout=5 * 60
                )
                if messages:
                    for message in messages:
                        content = json.loads(message.content)
                        return content.get('ContentURL')
        return False

    def post(self, request, *args, **kwargs):
        context = super(
            BlobRetreiverView,
            self).get_context_data(
            *
            args,
            **kwargs)
        form = self.form_class(self.request.POST)
        if form.is_valid():
            account_name = form.cleaned_data.get('account_name')
            queuename = form.cleaned_data.get('queuename')
            account_key = form.cleaned_data.get('account_key')
            blob_link = self.get_blob_link(
                account_key, queuename, account_name)
            context['blob_link'] = blob_link
            return render(request, self.template_name, context)
        context['form'] = form
        return render(request, self.template_name, context)
